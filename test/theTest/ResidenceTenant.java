package theTest;

import org.junit.*;

import models.Residence;
import models.Tenant;

import java.util.*;
import play.test.*;
import theTest.*;

public class ResidenceTenant extends UnitTest
{

  private Tenant tenant;
  private Residence residence;

  @Before
  public void setup()
  {
    residence = new Residence();
    residence.save();

    tenant = new Tenant("barney", "gumble", "barney@gumble.com");
    tenant.residence = residence;
    tenant.save();

    residence.tenant = tenant;
    residence.save();
  }

  @After
  public void teardown()
  {
    tenant.residence = null;
    tenant.save();
    residence.delete();
    tenant.delete();
  }

  @Test
  public void testCreate()
  {
    Tenant renter = Tenant.findByEmail("barney@gumble.com");
    // Positive test
    assertEquals(tenant.equals(renter), true);
    // Negative test
    assertNull(Tenant.findByEmail("marge@simpson.com"));
  }

}
