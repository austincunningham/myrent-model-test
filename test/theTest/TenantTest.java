package theTest;

import org.junit.*;

import models.Tenant;

import java.util.*;
import play.test.*;
import theTest.*;

public class TenantTest extends UnitTest
{

  private Tenant tenant;

  @Before
  public void setup()
  {
    tenant = new Tenant("barney", "gumble", "barney@gumble.com");
    tenant.save();
  }

  @After
  public void teardown()
  {
    tenant.delete();
  }

  @Test
  public void testCreate()
  {
    Tenant renter = Tenant.findByEmail("barney@gumble.com");
    // Positive test
    assertEquals(tenant.equals(renter), true);
    // Negative test
    assertNull(Tenant.findByEmail("marge@simpson.com"));
  }

}