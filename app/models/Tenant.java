package models;

import javax.persistence.Entity;
import play.db.jpa.Model;
import javax.persistence.OneToOne;

@Entity
public class Tenant extends Model
{

  
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  
  @OneToOne
  public Residence residence;

  public Tenant(String firstName, String lastName, String email)
  {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }

  public static Tenant findByEmail(String email)
  {
    return find("byEmail", email).first();
  }

  /**
   * For simplicity we choose to consider 2 Tenant objects equal if their emails match.
   * @param The Object (a Landlord object) to be compared to this object
   * @return True if the email of this object matches the argument's email.
   */
  @Override
  public boolean equals(Object o)
  {
    return email.equalsIgnoreCase(((Tenant)o).email);    
  }

}