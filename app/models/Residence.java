package models;

import javax.persistence.Entity;
import play.db.jpa.Model;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Residence extends Model
{
  @ManyToOne
  public Landlord landlord;
  
  @OneToOne(mappedBy = "residence")
  public Tenant tenant;
  
  // eircodes introduced for all RoI properties July 2015
  public String eircode;

  /**
   * Constructs a default Residence object. Field values chosen arbitrarily.
   */
  public Residence()
  {
    this.eircode = "X000 Y000"; // A non-existent eircode for test only
  }

  // Introduced for JUnit test
  public Residence(String eircode) 
  {
    this.eircode = eircode;
  }

  public static Residence findByEircode(String eircode)
  {
    return find("byEircode", eircode).first();
  }

  /**
   * For simplicity we choose to consider 2 Residence objects equal if their eircodes match.
   * @param The Object (a Residence object) to be compared to this object
   * @return True if the eircode of this object matches the argument's eircode.
   */
  @Override
  public boolean equals(Object o)
  {

    if (o instanceof Residence)
    {
      return eircode.equalsIgnoreCase(((Residence) o).eircode);
    }
    return false;
  }
}