package models;

import javax.persistence.Entity;
import play.db.jpa.Model;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;

@Entity
public class Landlord extends Model
{
  @OneToMany(mappedBy="landlord", cascade=CascadeType.ALL)
  public List<Residence> residences = new ArrayList<Residence>();
  public String firstName;
  public String lastName;

  public String email;
  public String password;

  public Landlord(String firstName, String lastName, String email) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    password = "secret";
  }

  public static Landlord findByEmail(String email)
  {
    return find("byEmail", email).first();
  }

  /**
   * For simplicity we choose to consider 2 Landlord objects equal if their emails match.
   * @param The Object (a Landlord object) to be compared to this object
   * @return True if the email of this object matches the argument's email.
   */
  @Override
  public boolean equals(Object o)
  {
    return email.equalsIgnoreCase(((Landlord)o).email);    
  }

}